package hr.ferit.iskoric.mysuperhotel;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Button btnHotel, btnRestaurants, btnCafes, btnHospital;
    int clikedBtnCaffees, clickedBtnnRestaurants, clickedBtnHospital;
    Marker m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        btnHotel=findViewById(R.id.hotel_location);
        btnCafes=findViewById(R.id.cafes);
        btnRestaurants=findViewById(R.id.restaurants);
        btnHospital=findViewById(R.id.hospital);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        btnHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LatLng hotel = new LatLng(45.550203, 18.695121);
                mMap.addMarker(new MarkerOptions().position(hotel).title("This is your hotel location!"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotel, 15f));
            }
        });

        btnRestaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickedBtnnRestaurants++;
                if((clickedBtnnRestaurants%2) == 0)
                {
                    m1.setVisible(false);
                    m2.setVisible(false);
                    m3.setVisible(false);
                    m4.setVisible(false);
                    m5.setVisible(false);
                }
                else
                {
                    LatLng restaurant1 = new LatLng(45.549166, 18.692707);
                m1=mMap.addMarker(new MarkerOptions().position(restaurant1).title("Karaka restaurant. Only 4 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurant1, 15f));

                LatLng restaurant2 = new LatLng(45.548079, 18.694698);
                m2=mMap.addMarker(new MarkerOptions().position(restaurant2).title("Muzej okusa restourant. Only 3 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurant2, 15f));

                LatLng restaurant3 = new LatLng(45.5506569, 18.693123);
                m3=mMap.addMarker(new MarkerOptions().position(restaurant3).title("McDonald's McDrive. Only 3 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurant3, 15f));

                LatLng restaurant4 = new LatLng(45.549179, 18.696529);
                m4=mMap.addMarker(new MarkerOptions().position(restaurant4).title("La Rosa restaurant. Only 4 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurant4, 15f));

                LatLng restaurant5 = new LatLng(45.549043, 18.695830);
                m5=mMap.addMarker(new MarkerOptions().position(restaurant5).title("Food Mljac restaurant. Only 3 minutes away").icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurant5, 15f));
                }





                //hotel.setVisible(false);

            }
        });

        btnHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickedBtnHospital++;

                if((clickedBtnHospital%2) == 0)
                {
                    m6.setVisible(false);
                    m7.setVisible(false);
                    m8.setVisible(false);
                    m9.setVisible(false);
                    m10.setVisible(false);
                    m11.setVisible(false);

                }
                else {
                    LatLng pharmacy = new LatLng(45.550984, 18.695894);
                    m6 = mMap.addMarker(new MarkerOptions().position(pharmacy).title("Drugstore, 2 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.pharmacy)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pharmacy, 15f));


                    LatLng swimming_pool = new LatLng(45.548353, 18.695218);
                    m7 = mMap.addMarker(new MarkerOptions().position(swimming_pool).title("Swimming pools, 2 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.swimming)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(swimming_pool, 15f));

                    LatLng bank = new LatLng(45.550855, 18.694988);
                    m8 = mMap.addMarker(new MarkerOptions().position(bank).title("Hrvatska poštanska banka (bank), 2 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.bank)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bank, 15f));

                    LatLng gas = new LatLng(45.549277, 18.692432);
                    m9 = mMap.addMarker(new MarkerOptions().position(gas).title("Gas station, 2 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.gas_station)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gas, 15f));

                    LatLng market = new LatLng(45.550079, 18.692598);
                    m10 = mMap.addMarker(new MarkerOptions().position(market).title("Spar market, 2 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.market)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(market, 15f));

                    LatLng postoffice = new LatLng(45.551136, 18.695026);
                    m11 = mMap.addMarker(new MarkerOptions().position(postoffice).title("Post office, 2 minutes away.").icon(BitmapDescriptorFactory.fromResource(R.drawable.postoffice)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(postoffice, 15f));
                }

            }
        });

        btnCafes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clikedBtnCaffees++;
                if((clikedBtnCaffees%2)==0)
                {
                    m12.setVisible(false);
                    m13.setVisible(false);
                }
                else
                {
                    LatLng caffee1 = new LatLng(45.550069, 18.694261);
                    m12=mMap.addMarker(new MarkerOptions().position(caffee1).title("Aktuell bar!").icon(BitmapDescriptorFactory.fromResource(R.drawable.coffee)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(caffee1, 15f));

                    LatLng caffee2 = new LatLng(45.550781, 18.695050);
                    m13=mMap.addMarker(new MarkerOptions().position(caffee2).title("Tango bar!").icon(BitmapDescriptorFactory.fromResource(R.drawable.coffee)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(caffee2, 15f));
                }


            }
        });
    }
}

package hr.ferit.iskoric.mysuperhotel;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UsersActivity extends AppCompatActivity {

    Button btnChat, btnMaps, btnSignOut, btnTaskFinished, btnReadContent;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    DatabaseReference myyref;
    FirebaseDatabase mydatabase;
    EditText userWritesRoomNumber;



    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthStateListener != null)
        {
            FirebaseAuth.getInstance().removeAuthStateListener(mAuthStateListener);
        }
    }

    private void setupFirebaseListener()
    {
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser myuser =firebaseAuth.getCurrentUser();
                if(myuser != null)
                {
                    //
                }
                else
                {
                    Intent intent=new Intent(UsersActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        };
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        mydatabase=FirebaseDatabase.getInstance();
        myyref=mydatabase.getReference("guest's requests");

        userWritesRoomNumber=findViewById(R.id.userWritesRoomNumber);
        btnReadContent=findViewById(R.id.readContent);
        btnChat=findViewById(R.id.button_chat);
        btnMaps=findViewById(R.id.button_maps);
        btnSignOut=findViewById(R.id.signout);
        btnTaskFinished=findViewById(R.id.taskfinised);
        setupFirebaseListener();

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UsersActivity.this, Chat.class));
            }
        });
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UsersActivity.this, MapsActivity.class));
            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
            }
        });

        btnReadContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UsersActivity.this, ReadContent.class));
            }
        });


        btnTaskFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkTaskState();
            }
        });
    }


    private void checkTaskState() {

        myyref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String userWrites=userWritesRoomNumber.getText().toString();
                if(dataSnapshot.exists()) {

                    int i=0;
                    for(i=0; i<1; i++)
                    {
                        if (dataSnapshot.child(userWrites).hasChild("done"))
                        {
                            myyref.child(userWrites).removeValue();
                            Toast.makeText(UsersActivity.this, "You requests are done!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(UsersActivity.this, "Sorry, we are not done yet.", Toast.LENGTH_SHORT).show();
                        }


                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}

package hr.ferit.iskoric.mysuperhotel;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;




public class EmployeeTasks extends AppCompatActivity {


    ListView mListView;
    FirebaseDatabase database;
    DatabaseReference ref;
    FirebaseDatabase database1;
    DatabaseReference ref1;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    EditText roomDone;
    Button updateBase;
    users user;
    public String a, b, c, d, e, f, g, h, i, j;
    public String value;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_tasks);

        user=new users();
        mListView=findViewById(R.id.listview);
        roomDone=findViewById(R.id.roomDone);
        updateBase=findViewById(R.id.sendToBase);
        database=FirebaseDatabase.getInstance();
        String roomIdDone = roomDone.getText().toString();
        ref=database.getReference("guest's requests").child(roomIdDone);
        database1=FirebaseDatabase.getInstance();
        ref1=database1.getReference("guest's requests");
        list = new ArrayList<>();
        adapter = new ArrayAdapter<>(this,R.layout.guest_info, R.id.textview, list);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        updateBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String roomIdDone = roomDone.getText().toString();

                ref1.child(roomIdDone).child("done").setValue("Done");
            }
        });
}


    private void showData(DataSnapshot dataSnapshot) {

        for(DataSnapshot snapshot : dataSnapshot.getChildren())
        {
            users user=snapshot.getValue(users.class);
            if(snapshot.hasChild("done"))
            {
                a=snapshot.child("done").getValue().toString();
            }
            else
            {
                a="";
            }

            if(snapshot.hasChild("alarmService"))
            {
                b=snapshot.child("alarmService").getValue().toString();
            }
            else
            {
                b="";
            }
            if(snapshot.hasChild("cleanSheets"))
            {
                c="Clean sheets";
            }
            else
            {
                c="";
            }
            if(snapshot.hasChild("cleaningService"))
            {
                d="Clean my room";
            }
            else
            {
                d="";
            }
            if(snapshot.hasChild("laundryService"))
            {
                e="Clean laundry";
            }
            else
            {
                e="";
            }
            if(snapshot.hasChild("miniBarRefill"))
            {
                f="MiniBar refill";
            }
            else
            {
                f="";
            }
            if(snapshot.hasChild("parking"))
            {
                g="Save me a parking lot";
            }
            else
            {
                g="";
            }
            if(snapshot.hasChild("roomService"))
            {
                h="Food room service";
            }
            else
            {
                h="";
            }
            if(snapshot.hasChild("takeMyPet"))
            {
                i="Pet walk";
            }
            else
            {
                i="";
            }
            if(snapshot.hasChild("taxiService"))
            {
                j="Taxi";
            }
            else
            {
                j="";
            }
            list.add(user.getRoomNumber() + "->room number" + "\n" + a +  "\n" + "Alarm time:"+ b + "\n" + c + "\n" + d +  "\n" + e + "\n" + f + "\n" + g +  "\n" + h + "\n" + i + "\n" + j);
            mListView.setAdapter(adapter);
        }

    }
}

package hr.ferit.iskoric.mysuperhotel;

import android.support.annotation.RequiresPermission;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ReadContent extends AppCompatActivity {


    ListView mListV;
    FirebaseDatabase mdb;
    DatabaseReference mdbref;
    ArrayList<String> mlist;
    ArrayAdapter<String> madapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_content);


        mListV=findViewById(R.id.mylistview);
        mdb=FirebaseDatabase.getInstance();
        mdbref=mdb.getReference("content");
        mlist=new ArrayList<>();
        madapter=new ArrayAdapter<>(this, R.layout.guest_info, R.id.textview, mlist);

        mdbref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void showData(DataSnapshot dataSnapshot) {

        for(DataSnapshot snapshot : dataSnapshot.getChildren())
        {

            String message=snapshot.child("text").getValue().toString();
            mlist.add(message);
            mListV.setAdapter(madapter);
        }

    }
}

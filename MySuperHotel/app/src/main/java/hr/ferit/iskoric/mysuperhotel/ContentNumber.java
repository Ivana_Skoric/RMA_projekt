package hr.ferit.iskoric.mysuperhotel;

public class ContentNumber {
    int id;
    String msg;

    public ContentNumber() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}


package hr.ferit.iskoric.mysuperhotel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class addContent extends AppCompatActivity {

    Button sendContent;
    EditText content;
    static int i=0;
    ContentNumber conNum = new ContentNumber();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_content);

        sendContent=findViewById(R.id.sendContent);
        content=findViewById(R.id.content);

        sendContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addContentToBase();
            }
        });

    }

    private void addContentToBase() {
        conNum.id++;
        DatabaseReference myDB;
        myDB = FirebaseDatabase.getInstance().getReference("content");
        String id=String.valueOf(conNum.id);
        String contentData=content.getText().toString();
        conNum.msg = contentData;
        myDB.child(id).child("text").setValue(conNum.msg);
    }
}

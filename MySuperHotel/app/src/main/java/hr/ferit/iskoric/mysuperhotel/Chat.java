package hr.ferit.iskoric.mysuperhotel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Chat extends AppCompatActivity {

    Button btnSendToBase;
    EditText mRoomNumber, alarm;
    DatabaseReference mDatabase;
    CheckBox cbtaxiService, cbroomService, cbcleaningRoomService, cbmyPetService, cbcleanLaundryService, cbparkingService, cbminibarService, cbcleanSheetService, cbalarmService;
    String taxiService, roomService, cleaningRoomService, myPetService, cleanLaundryService, parkingService, minibarService, cleanSheetService, alarmService;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat2);

        btnSendToBase=findViewById(R.id.send_data);
        mRoomNumber=findViewById(R.id.roomNumber);

        cbroomService=findViewById(R.id.roomService);
        cbtaxiService=findViewById(R.id.taxiService);
        cbcleaningRoomService=findViewById(R.id.cleaningService);
        cbmyPetService=findViewById(R.id.takeMyPetService);
        cbcleanLaundryService=findViewById(R.id.laundryService);
        cbparkingService=findViewById(R.id.parkingService);
        cbminibarService=findViewById(R.id.miniBarRefillService);
        cbcleanSheetService=findViewById(R.id.cleanSheetsService);
        cbalarmService=findViewById(R.id.alarmService);
        alarm=findViewById(R.id.alarmTime);

        btnSendToBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRequest();
            }
        });
    }




    public void addRequest()
    {
            String roomNumber = mRoomNumber.getText().toString();
            String alarmService = alarm.getText().toString();

            if(cbroomService.isChecked())
            {
                roomService= "Room service request";
            }
            if(cbtaxiService.isChecked())
            {
                taxiService= "Taxi service request";
            }
            if(cbcleaningRoomService.isChecked())
            {
                cleaningRoomService= "cleaningRoomService request";
            }
            if(cbmyPetService.isChecked())
            {
                myPetService= "mypet request";
            }
            if(cbcleanLaundryService.isChecked())
            {
                cleanLaundryService= "clean laundry request";
            }
            if(cbparkingService.isChecked())
            {
                parkingService= "parking request";
            }
            if(cbminibarService.isChecked())
            {
                minibarService= "refill minibar request";
            }
            if(cbcleanSheetService.isChecked())
            {
                cleanSheetService= "clean sheets request";
            }
//            if(cbalarmService.isChecked())
//            {
//                alarmService= "alarm request";
//            }
            String req1 = roomService;
            String req2 = taxiService;
            String req3 = cleaningRoomService;
            String req4 = myPetService;
            String req5 = cleanLaundryService;
            String req6 = parkingService;
            String req7 = minibarService;
            String req8 = cleanSheetService;
            String req9 = alarmService;

            mDatabase = FirebaseDatabase.getInstance().getReference("guest's requests").child(roomNumber);
            if(TextUtils.isEmpty(roomNumber))
            {
                Toast.makeText(Chat.this, "Enter your room number!", Toast.LENGTH_SHORT).show();
            }
            else
            {

                //String id = mDatabase.push().getKey();
                //String id = mDatabase.getKey();
                String id = mDatabase.getKey();
                users Guests = new users(id, roomNumber, req1, req2, req3, req4, req5, req6, req7, req8, req9);
                mDatabase.setValue(Guests);
                mRoomNumber.setText("");
                alarm.setText("");
                Toast.makeText(Chat.this, "Thanks, we received your requests and we'll do what you asked for as soon as possible!", Toast.LENGTH_SHORT).show();
            }
    }


}

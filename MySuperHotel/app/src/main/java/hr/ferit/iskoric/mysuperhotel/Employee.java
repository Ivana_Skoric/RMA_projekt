package hr.ferit.iskoric.mysuperhotel;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Employee extends AppCompatActivity {

    Button btnLogin;
    EditText inputEmail, inputPassword;
    private FirebaseAuth auth;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);

        btnLogin=findViewById(R.id.loginButton);
        inputEmail=findViewById(R.id.username);
        inputPassword=findViewById(R.id.password);

        auth=FirebaseAuth.getInstance();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email= inputEmail.getText().toString();
                final String password=inputPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(Employee.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        inputPassword.setError("Password too short.");
                                    }
                                    else {
                                        Toast.makeText(Employee.this, "Wrong password.", Toast.LENGTH_LONG).show();
                                    }
                                }
                                else {
                                    Toast.makeText(Employee.this, "Success, you're logged in.", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Employee.this, employee_choose.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(auth.getCurrentUser() != null)
        {
            String email = user.getEmail();
            if (email.equals("radim@radim.com"))
            {
                startActivity(new Intent(Employee.this, employee_choose.class));
            }

            else
            {
                startActivity(new Intent(Employee.this, MainActivity.class));
                Toast.makeText(this, email, Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "Access denied.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

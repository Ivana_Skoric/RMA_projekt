package hr.ferit.iskoric.mysuperhotel;

import android.widget.CheckBox;

public class users {

    private String userId;
    private String roomNumber;
    private String taxiService;
    private String roomService;
    private String cleaningService;
    private String takeMyPet;
    private String laundryService;
    private String parking;
    private String miniBarRefill;
    private String cleanSheets;
    private String alarmService;


    public users()
    {

    }
    public users(String userId, String roomNumber, String taxiService, String roomService, String cleaningService, String takeMyPet, String laundryService, String parking, String miniBarRefill, String cleanSheets, String alarmService) {
        this.userId = userId;
        this.roomNumber = roomNumber;
        this.taxiService = taxiService;
        this.roomService = roomService;
        this.cleaningService = cleaningService;
        this.takeMyPet = takeMyPet;
        this.laundryService = laundryService;
        this.parking = parking;
        this.miniBarRefill = miniBarRefill;
        this.cleanSheets = cleanSheets;
        this.alarmService = alarmService;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getTaxiService() {
        return taxiService;
    }

    public void setTaxiService(String taxiService) {
        this.taxiService = taxiService;
    }

    public String getRoomService() {
        return roomService;
    }

    public void setRoomService(String roomService) {
        this.roomService = roomService;
    }

    public String getCleaningService() {
        return cleaningService;
    }

    public void setCleaningService(String cleaningService) {
        this.cleaningService = cleaningService;
    }

    public String getTakeMyPet() {
        return takeMyPet;
    }

    public void setTakeMyPet(String takeMyPet) {
        this.takeMyPet = takeMyPet;
    }

    public String getLaundryService() {
        return laundryService;
    }

    public void setLaundryService(String laundryService) {
        this.laundryService = laundryService;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getMiniBarRefill() {
        return miniBarRefill;
    }

    public void setMiniBarRefill(String miniBarRefill) {
        this.miniBarRefill = miniBarRefill;
    }

    public String getCleanSheets() {
        return cleanSheets;
    }

    public void setCleanSheets(String cleanSheets) {
        this.cleanSheets = cleanSheets;
    }

    public String getAlarmService() {
        return alarmService;
    }

    public void setAlarmService(String alarmService) {
        this.alarmService = alarmService;
    }
}